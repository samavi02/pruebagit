<?php

/* Hola, esto es una modificacion */

class persona{
    public $nombre;
    public $edad;
    public $sexo;

}


class profesor extends persona{
    public $materia;
    public $presencia=20;

    function __construct($materia, $nombre, $edad, $sexo){
        $this->materia=$materia;
        $this->nombre=$nombre;
        $this->edad=$edad;
        $this->sexo=$sexo;
    }

}
class alumno extends persona{
    public $nota;
    public $presencia=50;

    function __construct($nota, $nombre, $edad, $sexo){
        $this->nota=$nota;
        $this->nombre=$nombre;
        $this->edad=$edad;
        $this->sexo=$sexo;
    }

    function mostrarDatos(){
        echo "<br>$this->nombre de $this->edad años, $this->sexo, con nota $this->nota";
    }

}

class aula{
    public $id;
    public $maxEstu;
    public $materia;

    function __construct($id, $maxEstu, $materia){
        $this->id=$id;
        $this->maxEstu=$maxEstu;
        $this->materia=$materia;
    }

}

function calculo($aula, $profesor, $alumnos){

    $faltas=0;

    foreach ($alumnos as $value) {
        if(rand(0,100)<$value->presencia){
            $faltas++;
        }
    }
    echo "<br>$faltas";

    if(rand(0,100) > (int) $profesor->presencia && $profesor->materia==$aula->materia && $aula->maxEstu/2>(count($alumnos)-$faltas)){
        echo "<br> Se puede dar clase en el $aula con el profesor $profesor";
    }else{
        echo "<br> No se puede dar clase en el $aula con el profesor $profesor";
    }

}

?>